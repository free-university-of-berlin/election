infile = "american-election-tweets.csv"

# clean data
import clean
clean.ansi2utf8(infile, "american-election-tweets-encoded.csv")

# import data into db
import loadData
loadData.intoDB("american-election-tweets-encoded.csv")