import codecs


# converts a give ansi file to utf8
def ansi2utf8(sourceFileName, targetFileName):
    with open(sourceFileName, mode='r', encoding="cp1252") as sourceFile:
        with open(targetFileName, mode='w', encoding="utf-8") as targetFile:
            targetFile.write(escapeQuotes(sourceFile.read()))


# escapes single quotes in a given string
def escapeQuotes(string):
    return string.replace("'", "''")
