import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import movie_reviews
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from psycopg2.extras import RealDictCursor
import psycopg2


# filter a list of words looking for the useful ones
def create_word_features(words):
    useful_words = [word for word in words if word not in stopwords.words("english")]
    my_dict = dict([(word, True) for word in useful_words])
    return my_dict


def testText(text):
    words = word_tokenize(text)
    words = create_word_features(words)
    classifier.prob_classify(words)
    c = classifier.prob_classify(words)
    return (c.prob('positive'), c.prob('negative'))


def main():
    # connect to database
    conString = open("config.ini").read()
    db = psycopg2.connect(conString)
    cursor = db.cursor(cursor_factory=RealDictCursor)

    # get tweets from database
    cursor.execute("SELECT text, id FROM election.tweets;")
    tweets = cursor.fetchall()

    # calculate negativity
    neg_reviews = []
    for i, fileid in enumerate(movie_reviews.fileids('neg')):
        print(i/len(movie_reviews.fileids('neg'))*100/2)
        words = movie_reviews.words(fileid)
        neg_reviews.append((create_word_features(words), "negative"))

    # calculate positivity
    pos_reviews = []
    for i, fileid in enumerate(movie_reviews.fileids('pos')):
        print(i / len(movie_reviews.fileids('pos')) * 100/2 +50)
        words = movie_reviews.words(fileid)
        pos_reviews.append((create_word_features(words), "positive"))

    # insert tweets into db
    for tweet in tweets:
        p, n = testText(tweet['text'])
        cursor.execute("UPDATE election.tweets SET positive = %s, negative = %s WHERE id = %s", (p, n, tweet['id']))
        db.commit()

if __name__ == "__main__":
    main()