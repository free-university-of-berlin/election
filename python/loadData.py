import psycopg2
import pandas
import re

# connect to database using connection string from config.ini file
conString = open("config.ini").read()
conn = psycopg2.connect(conString)
cursor = conn.cursor()


def intoDB(csvfile):
    # clear database
    print("clearing database")
    cursor.execute(open("create.sql").read())
    print("database cleared")

    # insert handles into database
    print("insert handles")
    dataframe = pandas.read_csv(csvfile, sep=';')
    handles = {}
    for row in dataframe.drop_duplicates(subset='handle').itertuples():
        cursor.execute("INSERT INTO election.handles (name) VALUES (%s) RETURNING id;", (row.handle,))
        handles.update({row.handle: cursor.fetchone()[0]})
    print("handles inserted")

    # insert tweets into database
    print("inserting tweets")
    values = []
    for row in dataframe.itertuples():
        values.append("('{}','{}','{}','{}','{}')".format(row.text, row.retweet_count, row.favorite_count, row.time, handles[row.handle]))
    cursor.execute("INSERT INTO election.tweets (text, retweet_count, favorite_count, time, handle_id) VALUES " + (','.join(values)))
    print("tweets inserted")

    # insert hashtags into database
    print("inserting hashtags")
    # selecting tweets for collecting hashtags
    cursor.execute("SELECT text FROM election.tweets")
    tweets = cursor.fetchall()
    hashtags = []
    for tweet in tweets:
        text = tweet[0]
        hashtags += re.findall("(#[a-zA-Z0-9]+)", text)

    #remove duplicates and insert into db
    hashtags= ",".join(list(set(["('{}')".format(hashtag) for hashtag in hashtags])))
    cursor.execute("INSERT INTO election.hashtags (text) VALUES " + hashtags)
    print("hashtags inserted")

    # build hashtag_tweet relation
    print("building hashtag_tweet relation")
    cursor.execute("SELECT id, text FROM election.tweets")
    tweets = cursor.fetchall()

    # get all hashtag, id pairs from db
    cursor.execute("SELECT text, id FROM election.hashtags")
    hashtags = dict(cursor.fetchall())
    tweets_hashtags= []
    for tweet in tweets:
        text = tweet[1]
        for tag in re.findall("(#[a-zA-Z0-9]+)", text):
            #find tag in list and get tag id
            tweets_hashtags.append((tweet[0], hashtags[tag]))

    tweets_hashtags= ",".join(list(set(["('{}','{}')".format(*tweet_hashtag) for tweet_hashtag in tweets_hashtags])))
    cursor.execute("INSERT INTO election.hashtag_tweet (tweet_id,hashtag_id) VALUES" + tweets_hashtags)
    print("builded hashtag_tweet realtion")
    
    # close cursor and commit changes
    cursor.close()
    conn.commit()
