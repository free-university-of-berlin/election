import time
import math
import flask
import pandas
import plotly as plotly
import psycopg2
from psycopg2.extras import RealDictCursor
import json
from sklearn.cluster import KMeans

from flask import Flask
from flask import render_template

app = Flask(__name__)


@app.route("/")
def home():
    return render_template('home.html')


@app.route('/json/')
@app.route('/json/<hashtags>')
def printJson(hashtags=['#maga', '#makeamericagreatagain']):
    # connect to database using connection string form config.ini file
    conString = open("config.ini").read()
    db = psycopg2.connect(conString)
    cursor = db.cursor(cursor_factory=RealDictCursor)

    # check for hashtags in list
    if type(hashtags) is list:
        pass
    else:
        hashtags=["#" + tag for tag in hashtags.split("&")]

    # execute query searching for hashtags
    cursor.execute("""SELECT count(election.tweets.id),
        CAST(make_date(CAST(date_part('YEAR',tweets.time)AS INT), cast(date_part('MONTH',tweets.time) AS INT),1) AS VARCHAR) as date, election.hashtags.text
        FROM election.tweets
        INNER JOIN election.hashtag_tweet ON election.tweets.id = election.hashtag_tweet.tweet_id
        INNER JOIN election.hashtags ON election.hashtag_tweet.hashtag_id = election.hashtags.id
        WHERE """ + " OR ".join(["election.hashtags.text ='"+tag+"'" for tag in hashtags])+"""
        GROUP BY DATE_PART('YEAR' , tweets.time), DATE_PART('MONTH' , tweets.time),election.hashtags.id
        ORDER BY date ASC
    """)

    # render template with content
    return render_template('json.html', **{'content': json.dumps(cursor.fetchall(), indent=2)})


@app.route('/kmeans/')
@app.route('/kmeans/<clusterCount>')
def kmeans(clusterCount = 5):
    clusterCount = int(clusterCount)

    if clusterCount < 1 or clusterCount > 9:
        clusterCount = 5

    # connect to database
    conString = open("config.ini").read()
    db = psycopg2.connect(conString)
    cursor = db.cursor(cursor_factory=RealDictCursor)

    # get max retweet_count from db
    cursor.execute("SELECT MAX(election.tweets.retweet_count) as max_retweet FROM election.tweets")
    max_retweet = cursor.fetchone()["max_retweet"]

    # get average positivity and average retweet_count
    cursor.execute("""SELECT election.hashtags.text, avg(election.tweets.positive) as positive, avg(election.tweets.retweet_count) As avgretweetcount
FROM election.hashtags
INNER JOIN election.hashtag_tweet ON election.hashtag_tweet.hashtag_id = election.hashtags.id
INNER JOIN election.tweets ON election.hashtag_tweet.tweet_id = election.tweets.id
GROUP BY election.hashtags.id;""")
    results = cursor.fetchall()

    # initialize DataFrame with query results
    df = pandas.DataFrame(
        [[
            row['positive'],
            row['avgretweetcount'] / max_retweet
        ] for row in results])

    # generate graph data using kmeans for cluster
    kmeans = KMeans(n_clusters=clusterCount).fit(df)
    point_x = []
    point_y = []
    point_text = []
    point_color = []
    colors = ["#ff0000", "#00ff00", "#0000ff", "ffff00", "ff00ff", "00ffff", "000000", "ffffff", "f0f0f0"]
    for i, item in enumerate(results):
        point_x.append(item['avgretweetcount'])
        point_y.append(item['positive'])
        cluster_id = kmeans.labels_[i]
        # point_text.append("blub")
        point_text.append(item['text'])
        point_color.append(colors[cluster_id])

    # generate graph out of graph data
    graphs = [{
        'data': [
            {
                'x': point_x,
                'y': point_y,
                'hovertext': point_text,
                'mode': 'markers',
                'type': 'scatter',
                'marker': {
                    'color': point_color
                }
            }
        ],
        'layout': {
            'title': 'average retweet count vs positivity',
            'hovermode': 'closest',
            'xaxis': {
                'title': 'average retweet count',
                'type': 'log',
                'autorange': True
            },
            'yaxis': {
                'title': 'Positivity by Sentimental Algorithm',
                'type': 'lin',
                'autorange': True
            },
            'width': 1100,
            'height': 800
        }
    }]

    # generate variable to pass to the html template
    ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]
    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)
    context = {'ids': ids, 'graphJSON': graphJSON}

    # render html template
    return flask.render_template('kmeans.html', **context)


# helper method to get a point using an angle and a radius
def pointFromAngle(angle, radius):
    x = math.cos(angle) * radius
    y = math.sin(angle) * radius
    return x, y


@app.route('/visual/')
def visualize():
    # connect to database
    conString = open("config.ini").read()
    db = psycopg2.connect(conString)
    cursor = db.cursor(cursor_factory=RealDictCursor)

    # execute query searching for hashtags that are not the only one in a tweet
    cursor.execute("""
    SELECT DISTINCT hs1.hashtag_id as id, hashtags.text as text
FROM election.hashtag_tweet AS hs1
INNER JOIN election.hashtag_tweet AS hs2 on hs1.tweet_id = hs2.tweet_id
INNER JOIN election.hashtags ON hs1.hashtag_id =hashtags.id
WHERE hs1.hashtag_id != hs2.hashtag_id;
    """)

    # generate dictionary for nodes
    dict = cursor.fetchall()
    dict2 = []
    for index, item in enumerate(dict):
        x, y = pointFromAngle(index / len(dict) * 360, 100)
        dict2.append({'id': item['id'], 'label': item['text'], 'x': x, 'y': y, 'size': 0.1})

    # execute query searching for hashtag connections using ids
    cursor.execute("""SELECT DISTINCT hs1.hashtag_id as source, hs2.hashtag_id as target
FROM election.hashtag_tweet AS hs1
INNER JOIN election.hashtag_tweet AS hs2 on hs1.tweet_id = hs2.tweet_id
WHERE hs1.hashtag_id != hs2.hashtag_id
ORDER BY hs1.hashtag_id, hs2.hashtag_id;""")

    # generate dictionary for connections
    dict = cursor.fetchall()
    dict3 = []
    for item in dict:
        dict3.append({'id': str(item['source']) + "-" + str(item['target']),
                      'label': str(item['source']) + "-" + str(item['target']), 'source': item['source'],
                      'target': item['target']})

    # generate final dictionary out of dictionaries
    dict = {'nodes': dict2, 'edges': dict3}

    # render template with content
    return render_template('visual.html', **{'content': json.dumps(dict)})


@app.route('/drawOne/')
@app.route('/drawOne/<hashtag>')
def drawOne(hashtag=['#maga']):
    # connect to database
    conString = open("config.ini").read()
    db = psycopg2.connect(conString)
    cursor = db.cursor(cursor_factory=RealDictCursor)

    # check for hashtags in list
    if type(hashtag) is list:
        pass
    else:
        hashtag = ["#" + tag for tag in hashtag.split("&")]

    # execute query searching for tweets with specific hashtag
    cursor.execute("""SELECT count(election.tweets.id) as likes,
        CAST(make_date(CAST(date_part('YEAR',tweets.time)AS INT), cast(date_part('MONTH',tweets.time) AS INT),1) AS VARCHAR) as date, election.hashtags.text
        FROM election.tweets
        INNER JOIN election.hashtag_tweet ON election.tweets.id = election.hashtag_tweet.tweet_id
        INNER JOIN election.hashtags ON election.hashtag_tweet.hashtag_id = election.hashtags.id
        WHERE """ + " OR ".join(["election.hashtags.text ='" + tag + "'" for tag in hashtag]) + """
        GROUP BY DATE_PART('YEAR' , tweets.time), DATE_PART('MONTH' , tweets.time),election.hashtags.id
        ORDER BY date ASC
    """)

    # generate graph
    raw = cursor.fetchall()
    graphs = [
        dict(
            data=[
                dict(
                    y=[i["likes"] for i in raw],
                    x=[i["date"] for i in raw],
                    type='bar'
                )
            ],
            layout=dict(
                title='{} usage history'.format(hashtag[0])
            )
        )
    ]
    ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]

    # Convert the figures to JSON
    # PlotlyJSONEncoder appropriately converts pandas, datetime, etc
    # objects to their JSON equivalents
    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)

    # render template with graph
    return render_template('barplot.html', ids=ids, graphJSON=graphJSON)


@app.route('/drawAll/')
def drawAll():
    # connect to database
    conString = open("config.ini").read()
    db = psycopg2.connect(conString)
    cursor = db.cursor(cursor_factory=RealDictCursor)

    # execute query searching for number of hastags per day
    cursor.execute("""SELECT count(election.hashtags.id) as count, to_char(election.tweets.time, 'YYYY-MM-DD') as date
FROM election.tweets
INNER JOIN election.hashtag_tweet ON election.hashtag_tweet.tweet_id = election.tweets.id
INNER JOIN election.hashtags ON election.hashtag_tweet.hashtag_id = election.hashtags.id
GROUP BY to_char(election.tweets.time, 'YYYY-MM-DD')
ORDER by to_char(election.tweets.time, 'YYYY-MM-DD')ASC;
    """)

    # create graph
    raw = cursor.fetchall()
    graphs = [
        dict(
            data=[
                dict(
                    y=[i["count"] for i in raw],
                    x=[i["date"] for i in raw],
                    type='bar'
                )
            ],
            layout=dict(
                title='hashtags used per day'
            )
        )

    ]
    ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]

    # Convert the figures to JSON
    # PlotlyJSONEncoder appropriately converts pandas, datetime, etc
    # objects to their JSON equivalents
    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)

    return render_template('barplot.html', ids=ids, graphJSON=graphJSON)


if __name__ == "__main__":
    app.run()
