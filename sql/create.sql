-- Recreate schema 'election'
DROP SCHEMA IF EXISTS election CASCADE;
CREATE SCHEMA IF NOT EXISTS election;

-- Recreate table 'handles'
CREATE TABLE IF NOT EXISTS election.handles
(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) UNIQUE
);

-- Recreate table 'hashtags'
CREATE TABLE IF NOT EXISTS election.hashtags
(
    id SERIAL NOT NULL PRIMARY KEY,
    text VARCHAR(255)
);

-- Recreate table 'tweets'
CREATE TABLE IF NOT EXISTS election.tweets
(
    id SERIAL NOT NULL PRIMARY KEY,
    text VARCHAR(255),
    retweet_count INT,
    favorite_count INT,
    time TIMESTAMP,
    handle_id INT REFERENCES election.handles (id)
);

-- Recreate relation table 'hashtag_tweet'
CREATE TABLE IF NOT EXISTS election.hashtag_tweet
(
    tweet_id INT REFERENCES election.tweets (id),
    hashtag_id INT REFERENCES election.hashtags (id)
);